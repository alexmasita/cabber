'use strict';

module.exports = function (environment) {
  let ENV = {
    modulePrefix: 'cabber',
    podModulePrefix: 'cabber/pods',
    environment,
    rootURL: '/',
    locationType: 'auto',
    EmberENV: {
      FEATURES: {
        // Here you can enable experimental features on an ember canary build
        // e.g. 'with-controller': true
      },
      EXTEND_PROTOTYPES: {
        // Prevent Ember Data from overriding Date.parse.
        Date: false
      }
    },
    APP: {
      // Here you can pass flags/options to your application instance
      // when it is created
    },
    firebase: {
      apiKey: 'AIzaSyCmQe4bsIXeQR8sppQF83MFLyc48lM0-8U',
      authDomain: 'mobile-services-9f146.firebaseapp.com',
      databaseURL: 'https://mobile-services-9f146.firebaseio.com',
      projectId: 'mobile-services-9f146',
      storageBucket: '<storage_bucket>',
      messagingSenderId: '741165857222'
    },
    torii: {
      sessionServiceName: 'session'
    }
  };
  ENV['g-map'] = {
    libraries: ['places', 'geometry'],
    key: 'AIzaSyDQps8Q5pGzcXMGAKpBqdmOe8iYoEL4bs8',
    version: 3
  }
  ENV['place-autocomplete'] = {
    exclude: true,
    key: 'AIzaSyAS4_1rvd0or02Q4K3fQx24YhuC6jxxGF0',
  };
  if (environment === 'development') {
    // ENV.APP.LOG_RESOLVER = true;
    // ENV.APP.LOG_ACTIVE_GENERATION = true;
    // ENV.APP.LOG_TRANSITIONS = true;
    // ENV.APP.LOG_TRANSITIONS_INTERNAL = true;
    // ENV.APP.LOG_VIEW_LOOKUPS = true;
  }

  if (environment === 'test') {
    // Testem prefers this...
    ENV.locationType = 'none';

    // keep test console output quieter
    ENV.APP.LOG_ACTIVE_GENERATION = false;
    ENV.APP.LOG_VIEW_LOOKUPS = false;

    ENV.APP.rootElement = '#ember-testing';
    ENV.APP.autoboot = false;
  }

  if (environment === 'production') {
    // here you can enable a production-specific feature
  }

  return ENV;
};
