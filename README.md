# cabber

This README outlines the details of collaborating on this Ember application.
A short introduction of this app could easily go here.

## Prerequisites

You will need the following things properly installed on your computer.

* [Git](https://git-scm.com/)
* [Node.js](https://nodejs.org/) (with npm)
* [Ember CLI](https://ember-cli.com/)
* [Google Chrome](https://google.com/chrome/)

## Installation

* Visit the following website and follow the instructions to install ember: [https://guides.emberjs.com/v2.18.0/getting-started/](https://guides.emberjs.com/v2.18.0/getting-started/).

* `git clone <repository-url>` this repository
* `cd cabber`
* `npm install`

## Running / Development

* `ember serve`
* Visit your app landing page at [http://localhost:4200](http://localhost:4200).
* Visit individual views here below to view your design progress:
  * [http://localhost:4200/client/bookrideinfo](http://localhost:4200/client/bookrideinfo).
  * [http://localhost:4200/client/choosecab](http://localhost:4200/client/choosecab).
  * [http://localhost:4200/client/fairandrate](http://localhost:4200/client/fairandrate).
  * [http://localhost:4200/client/locationselector](http://localhost:4200/client/locationselector).
  * [http://localhost:4200/client/profile](http://localhost:4200/client/profile).
  * [http://localhost:4200/client/schedule](http://localhost:4200/client/schedule).
  * [http://localhost:4200/driver/acceptrider](http://localhost:4200/client/acceptrider).
  * [http://localhost:4200/driver/cancelridereason](http://localhost:4200/client/cancelridereason).
  * [http://localhost:4200/driver/homeoffline](http://localhost:4200/client/homeoffline).
  * [http://localhost:4200/driver/locationselector](http://localhost:4200/client/locationselector).
  * [http://localhost:4200/driver/riderrequest](http://localhost:4200/client/riderrequest).


## Further Reading / Useful Links

* [ember.js](https://emberjs.com/)
* [ember-cli](https://ember-cli.com/)
* Development Browser Extensions
  * [ember inspector for chrome](https://chrome.google.com/webstore/detail/ember-inspector/bmdblncegkenkacieihfhpjfppoconhi)
  * [ember inspector for firefox](https://addons.mozilla.org/en-US/firefox/addon/ember-inspector/)
