import EmberRouter from '@ember/routing/router';
import config from './config/environment';

const Router = EmberRouter.extend({
  location: config.locationType,
  rootURL: config.rootURL
});

Router.map(function() {
  this.route('client', function() {
    this.route('locationselector');
    this.route('choosecab');
    this.route('schedule');
    this.route('login');
    this.route('bookrideinfo');
    this.route('fairandrate');
    this.route('profile');
    this.route('bookrideinfomaterial');
    this.route('choosecabswiper');
    this.route('loginfirebase');
    this.route('addcard');
    this.route('wheretogo');
    this.route('choosecabswipersimple');
    this.route('signup');
    this.route('splash');
  });

  this.route('driver', function() {
    this.route('locationselector');
    this.route('homeoffline');
    this.route('riderrequest');
    this.route('acceptrider');
    this.route('cancelridereason');
    this.route('cancelride');
    this.route('endtrip');
    this.route('menu');
    this.route('ratings');
  });

  this.route('public', function() {
    this.route('transport', function() {
      this.route('riders', function() {
        this.route('map', function() {});
      });
    });
  });
});

export default Router;
