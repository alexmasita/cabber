import InfoBox from 'info-box';
import GMapInfowindow from 'ember-g-map/components/g-map-infowindow';
import compact from 'ember-g-map/utils/compact';
import {
  isPresent
} from '@ember/utils';
import {
  observer
} from '@ember/object';
import {
  A
} from '@ember/array';
import google from 'google';

const allowedOptions = A(['disableAutoPan', 'maxWidth', 'pixelOffset', 'boxStyle']);


export default GMapInfowindow.extend({

  buildInfowindow() {
    if (google) {
      const infowindow = new InfoBox({
        content: this.get('element')
      });

      if (isPresent(this.get('attrs.onOpen'))) {
        infowindow.addListener('domready', () => this.handleOpenClickEvent());
      }

      if (isPresent(this.get('attrs.onClose'))) {
        infowindow.addListener('closeclick', () => this.handleCloseClickEvent());
      }
      return infowindow;
    }
  },
  optionsChanged: observer(...allowedOptions, function () {
    run.once(this, 'setOptions');
  }),

  setOptions() {
    const infowindow = this.get('infowindow');
    const options = compact(this.getProperties(allowedOptions));
    console.log('compact infobox options');
    console.log(options);
    if (isPresent(infowindow) && isPresent(Object.keys(options))) {
      infowindow.setOptions(options);
    }
  },
});
