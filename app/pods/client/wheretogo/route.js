import {
  inject as service
} from '@ember/service';
import Route from '@ember/routing/route';
import {
  get
} from '@ember/object';

export default Route.extend({
  session: service(),
  // async beforeModel() {
  //   if (get(this, 'session.isAuthenticated')) {
  //     return
  //   }
  //   return get(this, 'session').fetch().catch(() => this._sessionError());
  // },
  model() {
    if (!get(this, 'session.isAuthenticated')) {
      this.transitionTo('client.loginfirebase');
    }
  },
  actions: {
    signOut() {
      get(this, 'session').close().then(() => this._transitionToLoginwithFirebase());
    }
  },
  _catchSessionError() {},
  _transitionToLoginwithFirebase() {
    this.transitionTo('client.loginfirebase');
  },
  _sessionError() {}
});
