import {
  inject as service
} from '@ember/service';
import Route from '@ember/routing/route';
import {
  get
} from '@ember/object';

export default Route.extend({
  session: service(),
  beforeModel() {
    if (get(this, 'session.isAuthenticated')) {
      return;
    }
    return get(this, 'session').fetch().catch(() => this._catchSessionError());
  },
  model() {
    if (!get(this, 'session.isAuthenticated')) {
      this.transitionTo('client.loginfirebase');
    }
  },
  actions: {
    DriveWithCabber() {},
    signOut() {
      get(this, 'session').close().then(() => this._transitionToTopUserBenefits());
    }
  },
  _catchSessionError() {},
  _transitionToTopUserBenefits() {
    this.transitionTo('client.loginfirebase');
  }
});
