import {
  computed,
  get
} from '@ember/object';
import {
  inject as service
} from '@ember/service';
import Controller from '@ember/controller';
import firebase from 'firebase';
import firebaseui from 'firebaseui';

export default Controller.extend({
  session: service(),
  uiConfig: computed(function () {
    return {
      callbacks: {
        signInSuccess: this.signInSuccess(this)
      },
      credentialHelper: firebaseui.auth.CredentialHelper.NONE,
      signInOptions: [
        firebase.auth.EmailAuthProvider.PROVIDER_ID
      ]
    };
  }),
  signInSuccess(that) {
    return function (_currentUser, _credential, _redirectUrl) {
      // Do something.
      // Return type determines whether we continue the redirect automatically
      // or whether we leave that to developer to handle.
      get(that, 'session').fetch().then(() => that._transitionToPublic());
      return false;
    }
  },
  _transitionToPublic() {
    this.transitionToRoute('client.bookrideinfo');
  }
});
