import {
  inject as service
} from '@ember/service';
import Route from '@ember/routing/route';
import {
  get
} from '@ember/object';

export default Route.extend({
  session: service(),
  async beforeModel() {
    if (get(this, 'session.isAuthenticated')) {
      return
    }
    return get(this, 'session').fetch().catch(() => this._sessionError());
  },
  model() {
    console.log('session.isAuthenticated');
    console.log(get(this, 'session.isAuthenticated'));
    if (get(this, 'session.isAuthenticated')) {
      this.transitionTo('client.bookrideinfo');
    }
  },
  _sessionError() {}
});
