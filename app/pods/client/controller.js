import {
  equal
} from '@ember/object/computed';
import {
  inject as service
} from '@ember/service';
import Controller from '@ember/controller';
import {
  computed,
  get,
  set
} from '@ember/object';

export default Controller.extend({
  router: service(),
  locationsExpanded: equal('expandedItem', 'admin.locations.index'),
  categoriesExpanded: equal('expandedItem', 'admin.categories.index'),
  imagesExpanded: equal('expandedItem', 'admin.images.index'),
  expandedItem: computed('router.currentRouteName', function () {
    return get(this, 'router.currentRouteName');
  }),
  actions: {
    toggleExpandedItem(value, ev) {
      if (get(this, 'expandedItem') === value) {
        value = null;
      }
      set(this, 'expandedItem', value);
      ev.stopPropagation();
    },
  }
});
