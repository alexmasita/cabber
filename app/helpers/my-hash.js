import {
  helper
} from '@ember/component/helper';

export function myHash(params, hash) {
  return Object.assign({}, hash);
}

export default helper(myHash);
