(function () {
  function vendorModule() {
    'use strict';

    return {
      'default': self['InfoBox'],
      __esModule: true,
    };
  }

  define('info-box', [], vendorModule);
})();
